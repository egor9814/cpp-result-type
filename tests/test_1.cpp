//
// Created by egor9814 on 06.10.2021.
//

#include <stest.hpp>
#include <result_type.hpp>

AutoTestUnit(Test1)

AutoTestCase() {
	auto div = [](int a, int b) -> result<int> {
		if (b == 0)
			return error_of<int>("divide by zero");
		return a / b;
	};

	TestCheck(*div(1, 1).unwrap() == 1);
	TestCheck(*div(2, 1).unwrap() == 2);
	TestCheck(*div(1, 2).unwrap() == 0);
	TestCheck(*div(10, 2).unwrap() == 5);

	try {
		auto value = div(10, 0).unwrap();
		TestCheckMessage(value != value, "'10 / 0' unwrapped successful");
	} catch (const std::exception &) {}
}

EndTestUnit(Test1)
