//
// Created by egor9814 on 06.10.2021.
//

#ifndef RESULT_TYPE_RESULT_TYPE_HPP
#define RESULT_TYPE_RESULT_TYPE_HPP

#include <exception>
#include <stdexcept>
#include <string>
#include <memory>

template <typename T>
class result {
	static_assert(!std::is_base_of_v<std::exception, T>, "type T should not inherit a std::exception class");

	std::shared_ptr<T> mObject;
	std::string mError;

public:
	using error_t = std::exception;

	result(T object) : mObject(std::make_shared<T>(std::move(object))) {}

	explicit result(const error_t &err) : mError(err.what()) {}

	[[nodiscard]] bool isError() const {
		return !mError.empty();
	}

	[[nodiscard]] const auto &error() const {
		return mError;
	}

	[[nodiscard]] const std::shared_ptr<T> &object() const {
		return mObject;
	}

	[[nodiscard]] const std::shared_ptr<T> &unwrap() const {
		if (isError()) {
			throw std::runtime_error(mError);
		}
		return mObject;
	}
};

template <>
class result<void> {
	std::string mError;

public:
	using error_t = std::exception;

	explicit result(const error_t &err) : mError(err.what()) {}

	[[nodiscard]] bool isError() const {
		return !mError.empty();
	}

	[[nodiscard]]const auto &error() const {
		return mError;
	}

	void unwrap() const {
		if (isError()) {
			throw std::runtime_error(mError);
		}
	}
};

template <typename T>
result<T> result_of(const T &object) {
	return result<T>(object);
}

template <typename T = void>
result<T> error_of(const typename result<T>::error_t &error) {
	return result<T>(error);
}

template <typename T = void>
result<T> error_of(const std::string &error) {
	return result<T>(std::runtime_error(error));
}

template <typename T = void>
result<T> error_of(const char *error) {
	return result<T>(std::runtime_error(error));
}

#endif //RESULT_TYPE_RESULT_TYPE_HPP
